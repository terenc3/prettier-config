<!--
SPDX-FileCopyrightText: 2023 Benjamin Kahlau <terenc3@roanapur.de>

SPDX-License-Identifier: GPL-3.0-only
-->

# Prettier config

Sharable prettier config

## Installation

```shell
npm config set @terenc3:registry https://codeberg.org/api/packages/terenc3/npm/
npm i -D @terenc3/prettier-config
npm i -D prettier
npm i -D prettier-plugin-tailwindcss
```

### prettier.config.js

```js
export { default } from '@terenc3/prettier-config'
```

## Development

```sh
git clone git@codeberg.org:terenc3/prettier-config.git
make init
make
```
