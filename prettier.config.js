// SPDX-FileCopyrightText: 2023 Benjamin Kahlau <terenc3@roanapur.de>
//
// SPDX-License-Identifier: GPL-3.0-only

export default {
    printWidth: 180,
    trailingComma: 'none',
    tabWidth: 4,
    semi: false,
    singleQuote: true
}
