# SPDX-FileCopyrightText: 2023 Benjamin Kahlau <terenc3@roanapur.de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

NPM := npm
BIN := ./node_modules/.bin
PRETTIER := $(BIN)/prettier
PRETTIER_FLAGS := --cache --cache-strategy metadata --write
ESLINT := $(BIN)/eslint
ESLINT_FLAGS := --fix
SOURCES := $(wildcard *.md *.js)
SRC_JS := $(filter %.js, $(SOURCES))
SRC_MD := $(filter %.md, $(SOURCES))

all: node_modules format lint

init:
	find .git/hooks -type l -exec rm {} \;
	find .githooks -type f -exec ln -sf ../../{} .git/hooks/ \;

node_modules: package.json ## Install node packages
	$(NPM) prune && $(NPM) ci

format: $(SRC_JS) $(SRC_MD) | prettier.config.js ## Format code with prettier
	$(PRETTIER) $^  $(PRETTIER_FLAGS)

lint: $(SRC_JS) | eslint.config.js ## Lint code with eslint
	$(ESLINT) $^ $(ESLINT_FLAGS)

clean: ## Clean artifacts and node folder
	rm -rf node_modules

help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: all init clean format lint help
